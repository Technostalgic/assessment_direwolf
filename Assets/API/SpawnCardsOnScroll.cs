using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assessment
{
	[RequireComponent(typeof(ScrollRect))]
	public class SpawnCardsOnScroll : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private Card template = null;

		[SerializeField]
		private Sprite[] orderedSprites = null;

		private readonly Stack<Card> cardPool = new Stack<Card>();

		private readonly LinkedList<Card> usedCards = new LinkedList<Card>();

		private ScrollRect scrollRect = null;

		private float cardSpacing = 10;

		private float cardWidth = 135;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// calculate which cards need to be shown and make sure they are rendered, and remove
		/// cards that no longer need to be rendered
		/// </summary>
		public void UpdateVisibleCards()
		{
			int startIndex = GetLeftmostIndex();
			int cardCount = 6;

			// remove all the unnecessary cards and add the ones we need
			RemoveUnusedCards(startIndex, cardCount);
			AddNeededCards(startIndex, cardCount);
		}

		/// <summary>
		/// instantiate all the cards game objects that will be used
		/// </summary>
		public void InitializePool()
		{
			// remove the tamplate object from the scene
			template.gameObject.SetActive(false);

			// create 5 initial card instances to fill the object pool
			for (int i = 6; i > 0; i--)
			{
				Card card = Instantiate(template, scrollRect.content);
				card.name = "Card-" + cardPool.Count;
				RecycleCard(card);
			}
		}

		// Internal Utility: ----------------------------------------------------------------------

		/// <summary>
		/// removes all the cards that no longer need to be rendered and add them to the pool
		/// </summary>
		/// <param name="minIndex">the first card index that should be rendered</param>
		/// <param name="cardCount">how many cards should be rendered after the first</param>
		private void RemoveUnusedCards(int minIndex, int cardCount)
		{
			if (usedCards.Count <= 0) return;

			int maxIndex = minIndex + cardCount;

			// remove cards from front if they are not needed
			while (usedCards.First.Value.listIndex < minIndex)
			{
				RecycleCard(usedCards.First.Value);
				usedCards.RemoveFirst();
				if (usedCards.Count <= 0) return;
			}

			// remove tailing cards
			while (usedCards.Last.Value.listIndex >= maxIndex)
			{
				RecycleCard(usedCards.Last.Value);
				usedCards.RemoveLast();
				if (usedCards.Count <= 0) return;
			}
		}

		/// <summary>
		/// add all needed cards that are missing from the viewm given a starting index and
		/// card count
		/// </summary>
		/// <param name="minIndex">the first card index that should be rendered</param>
		/// <param name="cardCount">how many cards should be rendered after the first</param>
		private void AddNeededCards(int minIndex, int cardCount)
		{
			// ensure there will be at least one card in the list to build off of
			if (usedCards.Count <= 0) CreateStartingCard();

			int maxIndex = minIndex + cardCount;

			// add cards in front
			while (usedCards.First.Value.listIndex > minIndex)
			{
				Card card = GetCard();
				if (card == null) break;
				UpdateCardProperties(card, usedCards.First.Value.listIndex - 1);
				usedCards.AddFirst(card);
			}

			// add cards in back
			while (usedCards.Last.Value.listIndex < maxIndex - 1)
			{
				Card card = GetCard();
				if (card == null) break;
				UpdateCardProperties(card, usedCards.Last.Value.listIndex + 1);
				usedCards.AddLast(card);
			}
		}

		/// <summary>
		/// updates the specified card properties to match the properties that a card at the
		/// given index should have
		/// </summary>
		/// <param name="card">the card whose properties to modify</param>
		/// <param name="index">the list index which determines the card properties</param>
		private void UpdateCardProperties(Card card, int index)
		{
			card.listIndex = index;

			// calculate which sprite should be at the given index
			int spriteIndex = index % orderedSprites.Length;
			if (spriteIndex < 0) spriteIndex += orderedSprites.Length;

			// apply the card's image sprite
			card.Image.sprite = orderedSprites[spriteIndex];

			// position the card based on it's list index
			RectTransform cardTransform = card.transform as RectTransform;
			Vector3 pos = cardTransform.anchoredPosition;
			pos.x = (cardWidth + cardSpacing) * index;
			cardTransform.anchoredPosition = pos;
		}

		/// <summary>
		/// puts a card back into the pool and removes it from the scene
		/// </summary>
		private void RecycleCard(Card card) 
		{
			card.gameObject.SetActive(false);
			cardPool.Push(card);
		}

		/// <summary>
		/// gets a card from the pool without needing to create a new one
		/// </summary>
		private Card GetCard()
		{
			if (cardPool.Count > 0)
			{
				Card card = cardPool.Pop();
				card.gameObject.SetActive(true);
				return card;
			}
			return null;
		}

		private int GetLeftmostIndex()
		{
			float val = -scrollRect.content.anchoredPosition.x / (cardWidth + cardSpacing);
			return Mathf.FloorToInt(val);
		}

		private void CreateStartingCard()
		{
			Card firstCard = GetCard();
			UpdateCardProperties(firstCard, GetLeftmostIndex());
			usedCards.AddLast(firstCard);
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Awake()
		{
			// first, initialize the required data
			scrollRect = GetComponent<ScrollRect>();
			InitializePool();

			// create a card as a starting point for the linked list
			CreateStartingCard();
		}

		private void LateUpdate()
		{
			UpdateVisibleCards();
		}
	}
}
