using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assessment
{
	[RequireComponent(typeof(Image))]
	public class Card : MonoBehaviour
	{
		private Image image = null;
		
		public int listIndex = 0;

		public Image Image
		{
			get
			{
				if (image == null) image = GetComponent<Image>();
				return image;
			}
		}
	}
}